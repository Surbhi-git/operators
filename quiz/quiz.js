(function() {
    var questions = [{
      question: "Q1. Who developed R?",
      choices: ["Dennis Ritchie", "John Chambers", "Bjarne Stroustrup", "Arthur Whitney"],
      correctAnswer: "John Chambers"
    }, {
      question: "Q2. __ Programing language is the most widely used for data analysis and statistical modeling",
      choices: ["R","K","S","C"],
      correctAnswer: "R"
    }, {
      question: "Q3. Files containing R Script end with extension:",
      choices: [".Rp", ".S", ".R", "All of the above"],
      correctAnswer: ".R"
    },
     {
        question: "Q4. R runs on the ______ operating system        ",
        choices: ["ubuntu", "Windows", "linux", "Any operating system"],
        correctAnswer: "Any operating system"
      }, {
      question: "Q5. R is available as a free software under the ___ General Public Liscence    ",
      choices: ["A","B","C","GNU"],
      correctAnswer: "GNU"
    },{
      question: "Q6. R functionality is divided into a number of___",
      choices: ["Domain","Classes","Functions","Packages"],
      correctAnswer: "Packages"
    },{
      question: "Q7. Which one of the following is not a basic Data Type      ",
      choices: ["Integer","Data frame","Character","Numeric"],
      correctAnswer: "Data frame"
    }
  ];
    
    var questionCounter = 0; //Tracks question number
    var selections = []; //Array containing user choices
    var quiz = $('#quiz'); //Quiz div object
    
    // Display initial question
    displayNext();
    
    // Click handler for the 'next' button
    $('#next').on('click', function (e) {
      e.preventDefault();
      
      // Suspend click listener during fade animation
      if(quiz.is(':animated')) {        
        return false;
      }
      choose();
      
      // If no user selection, progress is stopped
      if (isNaN(selections[questionCounter])) {
        Swal.fire(
          {
             title:"ERROR!",
             html:"<h3 style='font-family:Open Sans;letter-spacing:5px'>Please make a selection!<br><h4>'You have not selected any option'.</h4></h3>",
             icon:"error",
          }
       )
      } else {
        questionCounter++;
        displayNext();
      }
    });
    
    // Click handler for the 'prev' button
    $('#prev').on('click', function (e) {
      e.preventDefault();
      
      if(quiz.is(':animated')) {
        return false;
      }
      choose();
      questionCounter--;
      displayNext();
    });
    
    // Click handler for the 'Start Over' button
    $('#start').on('click', function (e) {
      e.preventDefault();
      
      if(quiz.is(':animated')) {
        return false;
      }
      questionCounter = 0;
      selections = [];
      displayNext();
      $('#start').hide();
    });
    
    // Animates buttons on hover
    $('.button').on('mouseenter', function () {
      $(this).addClass('active');
    });
    $('.button').on('mouseleave', function () {
      $(this).removeClass('active');
    });
    
    // Creates and returns the div that contains the questions and 
    // the answer selections
    function createQuestionElement(index) {
      var qElement = $('<div>', {
        id: 'question'
      });
      
      var header = $('<h2>Question ' + (index + 1) + ':</h2>');
      qElement.append(header);
      
      var question = $('<p>').append(questions[index].question);
      qElement.append(question);
      
      var radioButtons = createRadios(index);
      qElement.append(radioButtons);
      
      return qElement;
    }
    
    // Creates a list of the answer choices as radio inputs
    function createRadios(index) {
      var radioList = $('<ul>');
      var item;
      var input = '';
      for (var i = 0; i < questions[index].choices.length; i++) {
        item = $('<li>');
        input = '<input type="radio" name="answer" value=' + i + ' />';
        input += questions[index].choices[i];
        item.append(input);
        radioList.append(item);
      }
      return radioList;
    }
    
    // Reads the user selection and pushes the value to an array
    function choose() {
      selections[questionCounter] = +$('input[name="answer"]:checked').val();
    }
    // Displays next requested element
    function displayNext() {


      quiz.fadeOut(function() {
        $('#question').remove();
        
        if(questionCounter < questions.length){
          var nextQuestion = createQuestionElement(questionCounter);
          quiz.append(nextQuestion).fadeIn();
          if (!(isNaN(selections[questionCounter]))) {
            $('input[value='+selections[questionCounter]+']').prop('checked', true);
          }
          
          // Controls display of 'prev' button
          if(questionCounter === 1){
            $('#prev').show();
          } else if(questionCounter === 0){
            
            $('#prev').hide();
            $('#next').show();
          }
        }else {
          var scoreElem = displayScore();
          quiz.append(scoreElem).fadeIn();
          $('#next').hide();
          $('#prev').hide();
          $('#start').show();
        }
      });
    }
    
    // Computes score and returns a paragraph element to be displayed
    function displayScore() {
      var score = $('<p>',{id: 'question'});
      
      var numCorrect = 0;
      for (var i = 0; i < selections.length; i++) {
        if (selections[i] ===   questions[i].choices.indexOf(questions[i].correctAnswer)) {
          numCorrect++;                  
        }
      }

      Swal.fire({
        title:      score.append('You got ' + numCorrect + ' questions out of ' +
        questions.length + ' right!!!'),
        icon:"success",
      })
      return score;
    }
  })();